package com.dinsaren.freepostsbackendserverapi.controllers;

import com.dinsaren.freepostsbackendserverapi.models.Category;
import com.dinsaren.freepostsbackendserverapi.models.Product;
import com.dinsaren.freepostsbackendserverapi.models.response.MessageResponse;
import com.dinsaren.freepostsbackendserverapi.services.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/product")
@Slf4j
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/list")
    public ResponseEntity<MessageResponse> getAllProduct() {
        log.info("Intercept get all products ");
        List<Product> productList = productService.getAllProduct();
        log.info("Get All products : {}", productList);
        return new ResponseEntity<MessageResponse>(new MessageResponse("Get Data Success", "Get Data Success", productList), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<MessageResponse> create(@RequestBody Product req) {
        log.info("Intercept crud product {}", req);
        productService.save(req);
        return new ResponseEntity<>(new MessageResponse("Create success", "Create Success", ""), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MessageResponse> getProductById(@PathVariable("id") Integer id) {
        log.info("Intercept get all products ");
        Product product = productService.getById(id);
        log.info("Get All products : {}", product);
        return new ResponseEntity<MessageResponse>(new MessageResponse("Get Data Success", "Get Data Success", product), HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<MessageResponse> delet(@RequestBody Product req) {
        log.info("Intercept delete product req {}", req);
        productService.delete(req);
        return new ResponseEntity<>(new MessageResponse("Delete product success", "Delete product success", null), HttpStatus.OK);
    }
}
