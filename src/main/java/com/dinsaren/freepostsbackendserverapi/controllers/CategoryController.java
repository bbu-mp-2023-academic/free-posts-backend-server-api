package com.dinsaren.freepostsbackendserverapi.controllers;

import com.dinsaren.freepostsbackendserverapi.models.Category;
import com.dinsaren.freepostsbackendserverapi.models.response.CategoryResponse;
import com.dinsaren.freepostsbackendserverapi.models.response.MessageResponse;
import com.dinsaren.freepostsbackendserverapi.services.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/category")
@Slf4j
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/list")
    public ResponseEntity<MessageResponse> getAllCategories() {
        List<Category> categoryList = categoryService.getAllCategory();
        MessageResponse messageResponse = new MessageResponse("Get Data Success", "Get Data Success", categoryList);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @GetMapping("/products/list")
    public ResponseEntity<MessageResponse> getAllCategoriesWithProducts() {
        List<CategoryResponse> categoryList = categoryService.getAllCategoryWithProducts();
        MessageResponse messageResponse = new MessageResponse("Get Data Success", "Get Data Success", categoryList);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<MessageResponse> create(@RequestBody Category req) {
        log.info("Intercept create category req {}", req);
        categoryService.create(req);
        return new ResponseEntity<>(new MessageResponse("Create Category success", "Create Category success", null), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MessageResponse> getCategoryById(@PathVariable("id") Integer id) {
        Category category = categoryService.getById(id);
        MessageResponse messageResponse = new MessageResponse("Get Data Success", "Get Data Success", category);
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<MessageResponse> update(@RequestBody Category req) {
        log.info("Intercept update category req {}", req);
        categoryService.create(req);
        return new ResponseEntity<>(new MessageResponse("Update Category success", "Update Category success", null), HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<MessageResponse> delet(@RequestBody Category req) {
        log.info("Intercept delete category req {}", req);
        categoryService.delete(req);
        return new ResponseEntity<>(new MessageResponse("Delete Category success", "Delete Category success", null), HttpStatus.OK);
    }


}
