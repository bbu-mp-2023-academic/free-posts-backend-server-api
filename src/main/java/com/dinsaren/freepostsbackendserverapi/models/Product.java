package com.dinsaren.freepostsbackendserverapi.models;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "products")
@Data
@ToString
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String nameKh;
    private Double price;
    private Double cost;
    private Double discount;
    private String imageUrl;
    private String status;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

}
