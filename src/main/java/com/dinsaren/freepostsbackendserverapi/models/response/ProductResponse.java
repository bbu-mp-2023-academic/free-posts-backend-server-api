package com.dinsaren.freepostsbackendserverapi.models.response;

import com.dinsaren.freepostsbackendserverapi.models.Category;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
@Data
@ToString
public class ProductResponse {
    private int id;
    private String name;
    private String nameKh;
    private Double price;
    private Double cost;
    private Double discount;
    private String imageUrl;
    private String status;

}
