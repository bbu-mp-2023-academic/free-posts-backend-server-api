package com.dinsaren.freepostsbackendserverapi.models.response;

import lombok.Data;

@Data
public class MessageResponse {
    private String code;
    private String message;
    private String messageKh;

    private Object data;

    public MessageResponse(String message, String messageKh, Object data) {
        this.code = "SUC-000";
        this.message = message;
        this.messageKh = messageKh;
        this.data = data;
    }


}
