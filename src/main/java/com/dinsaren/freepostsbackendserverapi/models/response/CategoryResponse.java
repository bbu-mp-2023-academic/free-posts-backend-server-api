package com.dinsaren.freepostsbackendserverapi.models.response;

import com.dinsaren.freepostsbackendserverapi.models.Product;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
@Getter
@Setter
public class CategoryResponse {
    private int id;
    private String name;
    private String nameKh;
    private String imageUrl;
    private String status;

    private List<ProductResponse> products;

}
