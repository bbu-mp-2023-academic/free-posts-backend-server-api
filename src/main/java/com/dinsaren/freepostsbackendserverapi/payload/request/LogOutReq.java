package com.dinsaren.freepostsbackendserverapi.payload.request;

public class LogOutReq {
    private Integer userId;

    public Integer getUserId() {
        return this.userId;
    }
}
