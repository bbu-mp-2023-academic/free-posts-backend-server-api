package com.dinsaren.freepostsbackendserverapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreePostsBackendServerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(FreePostsBackendServerApiApplication.class, args);
    }

}
