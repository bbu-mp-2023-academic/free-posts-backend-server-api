package com.dinsaren.freepostsbackendserverapi.repositories;

import com.dinsaren.freepostsbackendserverapi.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    List<Category> findByStatusInOrderByIdDesc(List<String> statusList);
    List<Category> findAllByStatusOrderByIdDesc(String status);
}
