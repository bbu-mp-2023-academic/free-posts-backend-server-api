package com.dinsaren.freepostsbackendserverapi.repositories;

import com.dinsaren.freepostsbackendserverapi.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    List<Product> findAllByCategory_Id(Integer id);
    List<Product> findAllByStatusInOrderByIdDesc(List<String> statusList);
}
