package com.dinsaren.freepostsbackendserverapi.services;

import com.dinsaren.freepostsbackendserverapi.constants.Constants;
import com.dinsaren.freepostsbackendserverapi.constants.ErrorCode;
import com.dinsaren.freepostsbackendserverapi.exception.AppException;
import com.dinsaren.freepostsbackendserverapi.models.User;
import com.dinsaren.freepostsbackendserverapi.repositories.UserRepository;
import com.dinsaren.freepostsbackendserverapi.security.services.UserDetailsImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class AuthenticationUtilService {
    private final UserRepository userRepository;

    public AuthenticationUtilService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User checkUser() throws AppException {
        log.info("Intercept get authentication");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            Optional<User> user = userRepository.findByPhoneNumberAndStatus(userDetails.getPhoneNumber(), Constants.STATUS_ACTIVE);
            if (user.isPresent()) {
                log.info("Get authentication success {}", authentication);
                return user.get();
            }
        } else {
            throw new AppException(HttpStatus.BAD_GATEWAY, ErrorCode.USER_NOT_PERMISSION, "User don't have permission");
        }

        return null;
    }

}
