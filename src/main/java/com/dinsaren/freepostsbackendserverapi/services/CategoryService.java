package com.dinsaren.freepostsbackendserverapi.services;

import com.dinsaren.freepostsbackendserverapi.models.Category;
import com.dinsaren.freepostsbackendserverapi.models.response.CategoryResponse;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategory();

    void create(Category req);

    Category getById(Integer id);

    void delete(Category req);

    List<CategoryResponse> getAllCategoryWithProducts();

}
