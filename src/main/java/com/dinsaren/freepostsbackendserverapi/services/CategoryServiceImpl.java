package com.dinsaren.freepostsbackendserverapi.services;

import com.dinsaren.freepostsbackendserverapi.models.Category;
import com.dinsaren.freepostsbackendserverapi.models.Product;
import com.dinsaren.freepostsbackendserverapi.models.response.CategoryResponse;
import com.dinsaren.freepostsbackendserverapi.models.response.ProductResponse;
import com.dinsaren.freepostsbackendserverapi.repositories.CategoryRepository;
import com.dinsaren.freepostsbackendserverapi.repositories.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository, ProductRepository productRepository) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
    }

    @Override
    public List<Category> getAllCategory() {
        List<String> statusList = new ArrayList<>();
        statusList.add("ACT");
        statusList.add("DEL");
        return categoryRepository.findByStatusInOrderByIdDesc(statusList);
    }

    @Override
    public void create(Category req) {
        if (req.getId() != 0) {
            Category category = getById(req.getId());
            if (category != null) {
                req.setStatus("ACT");
                categoryRepository.save(req);
            }
        } else {
            req.setStatus("ACT");
            categoryRepository.save(req);
        }
    }

    @Override
    public Category getById(Integer id) {
        return categoryRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(Category req) {
        if (req.getId() != 0) {
            Category category = getById(req.getId());
            if (category != null) {
                req.setStatus("DEL");
                categoryRepository.save(req);
            }
        }
    }

    @Override
    public List<CategoryResponse> getAllCategoryWithProducts() {
        List<CategoryResponse> categoryResponseList = new ArrayList<>();
        categoryRepository.findAllByStatusOrderByIdDesc("ACT").forEach(c->{
            CategoryResponse categoryResponse = new CategoryResponse();
            categoryResponse.setId(c.getId());
            categoryResponse.setName(c.getName());
            categoryResponse.setNameKh(c.getNameKh());
            categoryResponse.setStatus(c.getStatus());
            List<Product> product = productRepository.findAllByCategory_Id(c.getId());
            List<ProductResponse> productResponseList = new ArrayList<>();
            product.forEach(p->{
                ProductResponse productResponse = new ProductResponse();
                productResponse.setId(p.getId());
                productResponse.setName(p.getName());
                productResponse.setNameKh(p.getNameKh());
                productResponse.setCost(p.getCost());
                productResponse.setPrice(p.getPrice());
                productResponse.setDiscount(p.getDiscount());
                productResponse.setImageUrl(p.getImageUrl());
                productResponse.setStatus(p.getStatus());
                productResponseList.add(productResponse);
            });
            categoryResponse.setProducts(productResponseList);
            categoryResponseList.add(categoryResponse);
        });
        return categoryResponseList;
    }
}
