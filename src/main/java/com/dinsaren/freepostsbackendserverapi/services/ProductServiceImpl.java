package com.dinsaren.freepostsbackendserverapi.services;

import com.dinsaren.freepostsbackendserverapi.models.Product;
import com.dinsaren.freepostsbackendserverapi.repositories.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{
    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        List<String> status = new ArrayList<>();
        status.add("ACT");
        status.add("DEL");
        return productRepository.findAllByStatusInOrderByIdDesc(status);
    }

    @Override
    public void save(Product product) {
        productRepository.save(product);
    }

    @Override
    public Product getById(Integer id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(Product product) {
        if(getById(product.getId())!=null){
            product.setStatus("DEL");
            productRepository.save(product);
        }
    }
}
