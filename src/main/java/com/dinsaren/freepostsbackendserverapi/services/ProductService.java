package com.dinsaren.freepostsbackendserverapi.services;

import com.dinsaren.freepostsbackendserverapi.models.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();
    void save(Product product);

    Product getById(Integer id);

    void delete(Product product);
}
